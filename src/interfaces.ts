export interface CurrencyConfig {
  r030: number;
  txt: string;
  rate: number;
  cc: string;
  exchangedate: string;
}

export type CurrencyType = "UAN" | "USD" | "EUR" | "GBP" | "PLN";

export const Currencies: Array<CurrencyType> = [
  "USD",
  "EUR",
  "GBP",
  "PLN",
  "UAN",
];

export interface CurrencyData {
  [name: string]: CurrencyConfig | undefined;
}
