import React from "react";
import "./App.scss";
import { CurrencyConvertor } from "./components/CurrencyConverter";
import { CurrencyInfo } from "./components/CurrencyInfo";
import { DataFetcher } from "./DataFetcher";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <a href="https://docs.google.com/document/d/1KWuOkO2jl_K1Btm5iVaqiPt0YM2uUuXNcfYdTMI6ZG0/edit#">
          Currency convertor(objective)
        </a>
        <DataFetcher />
        <CurrencyInfo />
      </header>
      <div>
        <CurrencyConvertor />
      </div>
    </div>
  );
}

export default App;
