import axiosInstance from "axios";
import { CurrencyConfig } from "./interfaces";

export class ServerAPI {
  static getInitialData(): Promise<CurrencyConfig[]> {
    return axiosInstance
      .get<CurrencyConfig[]>(
        `https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json`
      )
      .then((result) => result.data);
  }
}
