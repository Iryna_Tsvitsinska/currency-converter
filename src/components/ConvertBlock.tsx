import { FC } from "react";
import { CurrencyType } from "../interfaces";
import { setValueUAN } from "../store/reducers/ValuesSlice";
import { useAppDispatch, useAppSelector } from "../store/store";

import { DropDownList } from "./DropDownList";

interface ConvertBlockConfig {
  initialCurrency: CurrencyType;
  onSelectCurrency(value: CurrencyType): void;
  label: string;
}
export const ConvertBlockItem: FC<ConvertBlockConfig> = (props) => {
  const { initialCurrency, onSelectCurrency, label } = props;
  const { currences } = useAppSelector((state) => state.currency);
  const { valueUAN } = useAppSelector((state) => state.value);
  const currencyData = currences[initialCurrency];
  const valueCurrentCurrency = currencyData
    ? (valueUAN / +currencyData!.rate).toFixed(2)
    : 0;
  const dispatch = useAppDispatch();
  if (!currences) {
    return null;
  }

  const selectCurrencyHandler = (value: CurrencyType): void => {
    onSelectCurrency(value);
  };
  const changeValueHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value;

    const valUAN = +(Number(value) * currencyData!.rate).toFixed(2);
    dispatch(setValueUAN(valUAN));
  };

  return (
    <div className="card convert-block-item">
      <div className=" d-flex justify-content-around">
        <div className="p-2">
          <label className="button-discription">{label}</label>
          <input
            className="form-control"
            type="number"
            id="value"
            placeholder="введіть суму..."
            onChange={changeValueHandler}
            value={parseFloat(valueCurrentCurrency.toString())}
          />
        </div>
        <DropDownList
          title="Валюта"
          initialValue={initialCurrency}
          setValue={selectCurrencyHandler}
        />
      </div>
    </div>
  );
};
