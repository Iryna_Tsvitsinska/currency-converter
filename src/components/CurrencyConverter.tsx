import { FC } from "react";
import { CurrencyType } from "../interfaces";
import {
  setMainCurrency,
  setSecondaryCurrency,
  swapCurrencies,
} from "../store/reducers/CurrencySlice";
import { useAppDispatch, useAppSelector } from "../store/store";
import { ConvertBlockItem } from "./ConvertBlock";
import icon from "../icon.png";

export const CurrencyConvertor: FC = () => {
  const { mainCurrency, secondaryCurrency } = useAppSelector(
    (state) => state.currency
  );
  const dispatch = useAppDispatch();

  const onSelectMainCurrencyHandler = (currency: CurrencyType) => {
    dispatch(setMainCurrency(currency));
  };
  const onSelectSecondaryCurrencyHandler = (currency: CurrencyType) => {
    dispatch(setSecondaryCurrency(currency));
  };

  const buttonHandler = (event: React.MouseEvent<HTMLButtonElement>) => {
    dispatch(swapCurrencies());
  };

  return (
    <div className="m-3">
      <div>Конвертер валют</div>

      <div className="convert-block row show-grid">
        <div className="row show-grid align-self-center justify-content-between">
          <div className="col-xs-12 col-md-5 p-2">
            <ConvertBlockItem
              initialCurrency={mainCurrency}
              onSelectCurrency={onSelectMainCurrencyHandler}
              label="В мене є"
            />
          </div>
          <div className="col-xs-12 col-md-2 p-2">
            <button className="btn btn-light" onClick={buttonHandler}>
              <img className="icon-swap" src={icon} alt="swap"></img>
            </button>
          </div>
          <div className="col-xs-12 col-md-5 p-2">
            <ConvertBlockItem
              initialCurrency={secondaryCurrency}
              onSelectCurrency={onSelectSecondaryCurrencyHandler}
              label="Отримаю"
            />
          </div>
        </div>
      </div>
    </div>
  );
};
