import { FC } from "react";
import { Currencies } from "../interfaces";

import { useAppSelector } from "../store/store";

export const CurrencyInfo: FC = () => {
  const { currences } = useAppSelector((state) => state.currency);

  if (!currences) {
    return <div>Сервер временно не доступен</div>;
  }
  return (
    <div className="currency-info">
      <h6>Офіційний курс гривні до іноземних валют за 1 одиницю</h6>
      <ul className="list-group list-group-flush">
        {Currencies.map((currency, index) => (
          <li key={index} className="list-group-item" aria-current="true">
            {currences[currency] && (
              <div className=" d-flex flex-row justify-content-between">
                <div className="">{currences[currency]!.txt}</div>
                <div className="">{currences[currency]!.rate}</div>
              </div>
            )}
          </li>
        ))}
      </ul>
    </div>
  );
};
