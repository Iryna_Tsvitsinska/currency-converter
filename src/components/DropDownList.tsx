import { Currencies, CurrencyType } from "../interfaces";

interface DropDownListPropsConfig {
  title: string;
  initialValue?: CurrencyType;
  setValue(value: CurrencyType): void;
}

const DefoultCurrency: CurrencyType = "UAN";

export const DropDownList = (props: DropDownListPropsConfig) => {
  const { title, initialValue = DefoultCurrency, setValue } = props;

  const setElemHandler = (e: React.FormEvent<HTMLSelectElement>) => {
    const value = e.currentTarget.value as CurrencyType;
    console.log(value);
    if (!value) {
      return null;
    }

    setValue(value);
  };

  return (
    <div className="p-2 ">
      <label className="label button-discription">{title}</label>
      <div className="select">
        <select onChange={setElemHandler} value={initialValue}>
          {Currencies.map((item, index) => (
            <option key={index} value={item}>
              {item}
            </option>
          ))}
        </select>
      </div>
    </div>
  );
};
