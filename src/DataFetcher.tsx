import { useEffect } from "react";
import { Currencies, CurrencyData } from "./interfaces";
import { ServerAPI } from "./ServerAPI";
import { setData, UANDATA } from "./store/reducers/CurrencySlice";
import { useAppDispatch } from "./store/store";

export const DataFetcher = () => {
  const dispatch = useAppDispatch();
  useEffect(() => {
    ServerAPI.getInitialData().then((data) => {
      const currencyData: CurrencyData = {};
      Currencies.forEach((currName) => {
        currencyData[currName] = data.find((curr) => curr.cc === currName);
      });
      currencyData["UAN"] = UANDATA;

      dispatch(setData(currencyData));
    });
  }, [dispatch]);
  return null;
};
