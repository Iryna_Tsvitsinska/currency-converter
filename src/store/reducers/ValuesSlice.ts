import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface InitialStateConfig {
  valueUAN: number;
}

const initialState: InitialStateConfig = {
  valueUAN: 0,
};

export const ValueSlice = createSlice({
  name: "Value",
  initialState,
  reducers: {
    setValueUAN(state, action: PayloadAction<number>) {
      state.valueUAN = action.payload;
    },
  },
});
export const { setValueUAN } = ValueSlice.actions;

export default ValueSlice.reducer;
