import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { CurrencyData, CurrencyType } from "../../interfaces";

interface InitialStateConfig {
  currences: CurrencyData;
  mainCurrency: CurrencyType;
  secondaryCurrency: CurrencyType;
}
export const UANDATA = {
  r030: 0,
  txt: "Українська гривна",
  rate: 1,
  cc: "UAN",
  exchangedate: "25.04.2022",
};

const initialState: InitialStateConfig = {
  currences: {},
  mainCurrency: "USD",
  secondaryCurrency: "UAN",
};

export const CurrencySlice = createSlice({
  name: "CurrencyData",
  initialState,
  reducers: {
    setData(state, action: PayloadAction<CurrencyData>) {
      state.currences = action.payload;
    },
    setMainCurrency(state, action: PayloadAction<CurrencyType>) {
      state.mainCurrency = action.payload;
    },
    setSecondaryCurrency(state, action: PayloadAction<CurrencyType>) {
      state.secondaryCurrency = action.payload;
    },
    swapCurrencies(state, action: PayloadAction) {
      const { mainCurrency, secondaryCurrency } = state;
      state.mainCurrency = secondaryCurrency;
      state.secondaryCurrency = mainCurrency;
    },
  },
});
export const {
  setData,
  setMainCurrency,
  setSecondaryCurrency,
  swapCurrencies,
} = CurrencySlice.actions;

export default CurrencySlice.reducer;
